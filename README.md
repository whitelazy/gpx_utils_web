GPX utils web

클리앙 간큰남자님이 만드신 gpx 관련코드들을 웹에서 사용할 수 있도록 변환한 프로그램입니다.

실행방법은 setup_module.py 실행 후 app.py 를 실행하면됩니다.

최신버전으로 업데이트하고싶으시면 setup_module.py의 실행옵션으로 저장소의 최신커밋해쉬를 입력하시면됩니다.
기본으로 설정된 저장소 주소는 https://bitbucket.org/whitelazy/입니다.

다른 저장소 받아서 실행하도록 변경하시려면 코드상에서 BASE_URL을 수정하면 되지만 간큰남자님 원본소스랑은 호환안되므로 수정된 소스보고 수정하셔야합니다.
사용법 및 수정방법은 코드보시면됩니다. 라이센스는 원 소스저자인 간큰남자님께서 지정한 라이센스를 따릅니다.
원 소스 저장소에서 확인하시기 바랍니다.


원본 소스
https://bitbucket.org/psijoy/gpx-course-profile-plotter/
https://bitbucket.org/psijoy/gpx2tcx/

수정한 소스
https://bitbucket.org/whitelazy/gpx-course-profile-plotter/
https://bitbucket.org/whitelazy/gpx2tcx/