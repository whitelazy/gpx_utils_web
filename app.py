import os
from flask import Flask, request, redirect, url_for, send_file
from werkzeug import secure_filename
from flask import send_from_directory
from gcpp.gpxcpp import do_plot_gpx_to_png
from gpx2tcx.gpx2tcx import convert_gpx_to_tcx

import io
import sys
from multiprocessing import Process, Queue

ALLOWED_EXTENSIONS = {'gpx'}

app = Flask(__name__)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def welcome_page():
    if request.method == 'POST':
        if 'plotter' in request.form:
            return redirect(url_for('upload_file'))
        elif 'tcx' in request.form:
            return redirect(url_for('upload_file_tcx'))
    return '''
    <!doctype html>
    <title>GPX Tools</title>
    <h1>GPX 고도 프로파일 생성 </h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=submit  name=plotter value=Go></p>
    </form>
    <br>
    <br>
    <h1>GPX -> TCX 변환</h1>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=submit  name=tcx value=Go></p>
    </form>
    <br>
    <br>
    <br>
    <br>
    <p>Made by <a target="_blank" href='https://bitbucket.org/psijoy'>간큰남자</a><br>
Packaged by <a target="_blank" href='https://bitbucket.org/whitelazy'>whitelazy</a><br>
<b>시킨사람 : muderer</b> </p>
'''


@app.route('/plotter', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename), code=307)
    return '''
    <!doctype html>
    <title>GPX 고도 프로파일 생성</title>
    <h1>GPX 고도 프로파일 생성</h1>
    <h3>Upload gpx File</h3>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload></p>
      <p><input type=checkbox name=rotate value=30> text rotation 30 degree</p>
    </form>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p>Made by <a target="_blank" href='https://bitbucket.org/psijoy/gpx-course-profile-plotter'>간큰남자</a><br>
Packaged by <a target="_blank" href='https://bitbucket.org/whitelazy/gpx-course-profile-plotter'>whitelazy</a><br>
<b>시킨사람 : muderer</b> </p>
    '''
# <p>text rotation : <input type="number" min="0" max="180" value="0" name=rotate></p>


def run_process(queue, data, filename, rotate, colorize):
    queue.put(do_plot_gpx_to_png(data, filename, rotate=rotate, colorize=colorize))


@app.route('/plotter/uploads/<filename>', methods=['GET', 'POST'])
def uploaded_file(filename):
    if request.method == 'POST':
        file = request.files['file']
        rotation = 0
        if 'rotate' in request.form:
            rotation = request.form['rotate']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            q = Queue()
            p = Process(target=run_process, args=(q, file.stream._file.read().decode('UTF-8'), filename, rotation, 'y'))
            p.start()

            (length, height, image) = q.get()
            p.join()

            print("Length: {}, Height: {}".format(length, height))
            return send_file(io.BytesIO(image), attachment_filename=filename, mimetype='image/png')
    return ''


@app.route('/gpx2tcx', methods=['GET', 'POST'])
def upload_file_tcx():
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file_tcx',
                                    filename=filename), code=307)
    return '''
    <!doctype html>
    <title>GPX -> TCX 변환</title>
    <h1>GPX -> TCX 변환</h1>
    <h3>Upload gpx File</h3>
    <form action="" method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload></p>
      <p><input type=number name=speed value=20> Average Speed</p>
    </form>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p>Made by <a target="_blank" href='https://bitbucket.org/psijoy/gpx2tcx'>간큰남자</a><br>
Packaged by <a target="_blank" href='https://bitbucket.org/whitelazy/gpx2tcx'>whitelazy</a><br>
<b>시킨사람 : muderer</b> </p>
'''


@app.route('/gpx2tcx/uploads/<filename>', methods=['GET', 'POST'])
def uploaded_file_tcx(filename):
    if request.method == 'POST':
        file = request.files['file']
        speed = 20
        if 'speed' in request.form:
            speed = int(request.form['speed'])
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)

            tcx = convert_gpx_to_tcx(file.stream._file.read().decode('UTF-8'), filename, speed)

            return send_file(io.BytesIO(str.encode(tcx)), attachment_filename=filename[:-3]+'tcx', mimetype='text/xml'
                             , as_attachment=True)
    return ''

port = 5000
if len(sys.argv) >= 2:
    port = int(sys.argv[1])

app.run(host='0.0.0.0', port=port, threaded=True)
