import argparse
import os
import shutil
import subprocess

BASE_URL = 'https://bitbucket.org/whitelazy/{}/get/{}.tar.gz'


def parse_args():
    args = argparse.ArgumentParser()
    args.add_argument('-p', '--gpx_plotter', help='source repo commit hash', default='HEAD')
    args.add_argument('-t', '--gpx2tcx', default='HEAD')
    return args.parse_args()


def download_module(module, commit, dest):
    if os.path.exists(dest):
        shutil.rmtree(dest)
    os.mkdir(dest)

    os.system('wget -q {}'.format(BASE_URL.format(module, commit)))
    os.system('tar -zxf {}.tar.gz --strip-component=1 -C {}'.format(commit, dest))
    os.remove('{}.tar.gz'.format(commit))

    if os.path.exists('{}/requirements.txt'.format(dest)):
        os.system('pip install -r {}/requirements.txt'.format(dest))


def main():
    args = parse_args()
    if args.gpx_plotter:
        download_module('gpx-course-profile-plotter', args.gpx_plotter[:12], 'gcpp')

    if args.gpx2tcx:
        download_module('gpx2tcx', args.gpx2tcx[:12], 'gpx2tcx')


if __name__ == "__main__":
    main()